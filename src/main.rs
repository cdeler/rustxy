extern crate tokio;

use std::fmt;

use tokio::io;
use tokio::net::TcpListener;
use tokio::prelude::*;

fn main() {
    let addr = "127.0.0.1:8080".parse().unwrap();
    let listener = TcpListener::bind(&addr).unwrap();

    let server = listener
        .incoming()
        .for_each(|socket| {
            println!("accepted socket; addr={:?}", socket.peer_addr().unwrap());


            let mut x = 0;

            while x < 2147483647 {
                x += 1;
            }

            let response = fmt::format(format_args!("Hello world {}\n", x));

            // Process socket here.
            let connection = io::write_all(socket, response).then(|res| {
                println!("wrote message; success={:?}", res.is_ok());
                Ok(())
            });

            // Spawn a new task that processes the socket:
            tokio::spawn(connection);
            Ok(())
        }).map_err(|err| {
            // All tasks must have an `Error` type of `()`. This forces error
            // handling and helps avoid silencing failures.
            //
            // In our example, we are only going to log the error to STDOUT.
            println!("accept error = {:?}", err);
        });

    println!("Running the server");

    tokio::run(server);
}
